package server;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;

import java.awt.Color;

import javax.swing.JLabel;

import java.awt.Font;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JCheckBox;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.border.LineBorder;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JRadioButton;


import javax.swing.JToggleButton;

public class Configure {

	private static JFrame frmFingerprintServer;
	private static JTextField inputPort;
	private static JTextField inputPath;
	private static JCheckBox chckSaveLogs;
	private static Integer port;
	private static JRadioButton rdEnabled;
	private static JRadioButton rdDisabled;
	private static final String fileJar = "fingerprint-3.0.jar";
	private static final String fileLog = "log_fingerprint.log";
	private static final String fileBat = "runServer.cmd";
	private static final String nameTask = "Fingerprint Server";

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Configure window = new Configure();
					window.frmFingerprintServer.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Configure() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmFingerprintServer = new JFrame();
		frmFingerprintServer.setTitle("Fingerprint Server");
		frmFingerprintServer.getContentPane().setBackground(Color.LIGHT_GRAY);
		frmFingerprintServer.getContentPane().setForeground(new Color(102, 153, 204));
		frmFingerprintServer.setResizable(false);
		frmFingerprintServer.setBounds(100, 100, 486, 285);
		//frmFingerprintServer.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmFingerprintServer.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		frmFingerprintServer.getContentPane().setLayout(null);
		
		
		frmFingerprintServer.addWindowListener(new java.awt.event.WindowAdapter() {
		    @Override
		    public void windowClosing(java.awt.event.WindowEvent windowEvent) {
		    	closeWindows();
		    }
		});

		JPanel panel = new JPanel();
		panel.setBorder(null);
		panel.setBackground(Color.LIGHT_GRAY);
		panel.setBounds(124, 11, 221, 27);
		frmFingerprintServer.getContentPane().add(panel);

		JLabel lblFingerprintServerV = new JLabel("Fingerprint Server V3");
		lblFingerprintServerV.setFont(new Font("Gadugi", Font.BOLD, 14));
		panel.add(lblFingerprintServerV);

		JButton btnClose = new JButton("Cerrar");
		btnClose.setBackground(Color.LIGHT_GRAY);
		btnClose.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				closeWindows();
			}
		});
		btnClose.setBounds(140, 222, 89, 23);
		frmFingerprintServer.getContentPane().add(btnClose);

		final JPanel panel_1 = new JPanel();
		panel_1.setBorder(new LineBorder(Color.GRAY, 1, true));
		panel_1.setBackground(new Color(204, 204, 204));
		panel_1.setBounds(23, 49, 430, 152);
		frmFingerprintServer.getContentPane().add(panel_1);
		panel_1.setLayout(null);

		JLabel labelPort = new JLabel("Puerto:");
		labelPort.setBounds(25, 54, 47, 18);
		panel_1.add(labelPort);
		labelPort.setFont(new Font("Yu Gothic UI Semilight", Font.PLAIN, 13));

		inputPort = new JTextField();
		inputPort.setBounds(82, 54, 86, 20);
		inputPort.setText("8080");
		panel_1.add(inputPort);
		inputPort.setColumns(10);
		
		rdEnabled = new JRadioButton("Activar Tarea");
		rdEnabled.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent arg0) {
				try {
					String out;
					out = enableTask(true);
					if(out.isEmpty()) {
						throw new Exception("Acceso Denegado.");
					}
					rdDisabled.setSelected(false);
					JOptionPane.showMessageDialog(null, out, "Tarea Automatica", JOptionPane.INFORMATION_MESSAGE);
				} catch (Exception e) {
					rdEnabled.setSelected(false);
					JOptionPane.showMessageDialog(null, e.getMessage(), "Error de Sistema", JOptionPane.INFORMATION_MESSAGE);
				}
			}
		});
		rdEnabled.setEnabled(false);
		rdEnabled.setBackground(Color.LIGHT_GRAY);
		rdEnabled.setBounds(253, 44, 146, 18);
		panel_1.add(rdEnabled);
		
		rdDisabled = new JRadioButton("Desactivar Tarea");
		rdDisabled.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent arg0) {
				try {
					String out;
					out = enableTask(false);
					if(out.isEmpty()) {
						throw new Exception("Acceso Denegado.");
					}
					rdEnabled.setSelected(false);
					JOptionPane.showMessageDialog(null, out, "Tarea Automatica", JOptionPane.INFORMATION_MESSAGE);
				} catch (Exception e) {
					rdDisabled.setSelected(false);
					JOptionPane.showMessageDialog(null, e.getMessage(), "Error de Sistema", JOptionPane.INFORMATION_MESSAGE);
				}
			}
		});
		rdDisabled.setEnabled(false);
		rdDisabled.setBackground(Color.LIGHT_GRAY);
		rdDisabled.setBounds(253, 70, 146, 23);
		panel_1.add(rdDisabled);

		final JCheckBox chckAutomatic = new JCheckBox("Iniciar Automaticamente");
		chckAutomatic.setToolTipText("Crea o Reemplaza una Tarea de Ejecucion Diaria");
		chckAutomatic.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent arg0) {
				if(chckAutomatic.isSelected()) {
					try {
						String strExec = strRunner();
						final String fileLogDate = "log_fingerprint_%date:/=%_%time::=%.log";
						fileRunServer(strExec.replace(fileLog, fileLogDate));
						String task = "SchTasks /Create /SC ONSTART /TN \""+nameTask+"\" /TR \""+ getOutPut(runTime("cd")) +"\\"+fileBat+"\" /F";
						String out = getOutPut(runTime(task));
						if(out.isEmpty()) {
							throw new Exception("Acceso Denegado.");
						}
						rdDisabled.setEnabled(true);
						rdEnabled.setEnabled(true);
						JOptionPane.showMessageDialog(null, out, "Tarea Automatica", JOptionPane.INFORMATION_MESSAGE);
						rdEnabled.setSelected(true);
					} catch (Exception e) {
						chckAutomatic.setSelected(false);
						rdDisabled.setEnabled(false);
						rdEnabled.setEnabled(false);
						JOptionPane.showMessageDialog(null, "Descripción: " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
					}
				} else {
					try {
						JOptionPane.showMessageDialog(null, deleteTask(), "Sucess", JOptionPane.INFORMATION_MESSAGE);
						rdDisabled.setEnabled(false);
						rdEnabled.setEnabled(false);
						rdEnabled.setSelected(false);
						rdDisabled.setSelected(false);
					} catch (IOException e) {
						chckAutomatic.setSelected(true);
						JOptionPane.showMessageDialog(null, "Descripción: " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
					}
					
				}
					
			}
		});
		chckAutomatic.setBounds(222, 11, 177, 27);
		panel_1.add(chckAutomatic);
		chckAutomatic.setBackground(Color.LIGHT_GRAY);
		chckAutomatic.setFont(new Font("Yu Gothic UI Light", Font.PLAIN, 13));

		chckSaveLogs = new JCheckBox("Guardar Logs");
		chckSaveLogs.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent arg0) {
				if(chckSaveLogs.isSelected() && inputPath.getText().isEmpty()) {
					try {
						inputPath.setText(getOutPut(runTime("cd")));
					} catch (IOException e) {
						JOptionPane.showMessageDialog(null, "Descripción: " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
					}
				}
			}
		});
		chckSaveLogs.setBounds(23, 118, 97, 27);
		panel_1.add(chckSaveLogs);
		chckSaveLogs.setBackground(Color.LIGHT_GRAY);
		chckSaveLogs.setFont(new Font("Yu Gothic UI Light", Font.PLAIN, 13));

		inputPath = new JTextField();
		inputPath.setBounds(126, 122, 176, 20);
		panel_1.add(inputPath);
		inputPath.setColumns(10);

		JButton btnSelecc = new JButton("Seleccionar");
		btnSelecc.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				inputPath.setText(getPathLog(panel_1));
				chckAutomatic.setSelected(false);
			}
		});
		btnSelecc.setBackground(Color.LIGHT_GRAY);
		btnSelecc.setBounds(312, 121, 87, 23);
		panel_1.add(btnSelecc);
		
		final JToggleButton tglbtnIniciar = new JToggleButton("Iniciar");
		tglbtnIniciar.setBackground(Color.LIGHT_GRAY);
		tglbtnIniciar.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent arg0) {
				if(tglbtnIniciar.isSelected()) {
					try {
						String strRunn = strRunner();
						tglbtnIniciar.setText("Detener");
						SimpleDateFormat format = new SimpleDateFormat("ddMMyyyy HHmmss");
						String dateString = format.format( new Date());
						String logDate = fileLog.replace(".log", "_") + dateString + ".log";
						runTime(strRunn.replace(fileLog, logDate));
						inputPath.setEnabled(false);
						inputPort.setEnabled(false);
						chckSaveLogs.setEnabled(false);
						JOptionPane.showMessageDialog(null, "Servidor en linea, puerto: " + port, "Sucess", JOptionPane.INFORMATION_MESSAGE);
					} catch (Exception e) {
						tglbtnIniciar.setSelected(false);
						tglbtnIniciar.setText("Iniciar");
						inputPath.setEnabled(true);
						inputPort.setEnabled(true);
						chckSaveLogs.setEnabled(true);
						JOptionPane.showMessageDialog(null, "Descripción: " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
					}
				} else {
					runTime("taskkill /F /IM java.exe");
					tglbtnIniciar.setText("Iniciar");
					inputPath.setEnabled(true);
					inputPort.setEnabled(true);
					chckSaveLogs.setEnabled(true);
					JOptionPane.showMessageDialog(null, "Servidor detenido", "Sucess", JOptionPane.INFORMATION_MESSAGE);
				}
			}
		});
		tglbtnIniciar.setBounds(239, 222, 105, 23);
		frmFingerprintServer.getContentPane().add(tglbtnIniciar);



	}

	private static boolean setPort() {
		boolean valid = true;
		try {
			port = Integer.valueOf(inputPort.getText());
		} catch (NumberFormatException e) {
			valid = false;
		}
		return valid;
	}

	private static Process runTime(final String command) {
		Process p = null;
		try 
		{ 
			p = Runtime.getRuntime().exec ("cmd /c " + command); 
		} 
		catch (Exception e) 
		{ 
			e.printStackTrace();
		}
		return p;
	}

	private static String getPathLog(JPanel panel) {
		JFileChooser fc=new JFileChooser();
		String path = null;
		fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		int seleccion=fc.showOpenDialog(panel);
		if(seleccion==JFileChooser.APPROVE_OPTION){
			File fichero=fc.getSelectedFile();
			path =  fichero.getAbsolutePath();
		}
		return path;
	}

	private static boolean existFile(final String file) {
		return new File(file).exists();
	}

	private static boolean isFolder(final String folder) {
		File _folder = new File(folder);
		return _folder.exists() && _folder.isDirectory();
	}

	private static String strRunner() throws Exception {
		String runnJar = null;
		if(existFile(fileJar)) {
			if(setPort()) {
				runnJar = "java -Dserver.port="+port+" -jar \"" + getOutPut(runTime("cd")) +"\\"+ fileJar + "\" ";
				if(chckSaveLogs.isSelected()) {
					if(isFolder(inputPath.getText())) {
						runnJar += "> \""+inputPath.getText()+"\\"+ fileLog + "\" 2>&1 &";
					} else {
						throw new Exception("Directorio Inválido");
					}
				}
			} else {
				throw new Exception("Puerto no válido.");
			}
		} else {
			throw new Exception("No se encuentra el archivo "+fileJar);
		}
		return runnJar;
	}
	
	private static String getOutPut(Process p) throws IOException {
		String out = "";
		
        InputStream is = p.getInputStream(); 
         
        BufferedReader br = new BufferedReader (new InputStreamReader (is)); 
         
        String aux = br.readLine(); 
         
        while (aux!=null) 
        { 
            out += aux;
            aux = br.readLine();
        }
        return out;
	}
	
	private static void fileRunServer(final String content) throws Exception {
		 try {
	        File file = new File(fileBat);
	        file.createNewFile();
	        FileWriter fw = new FileWriter(file);
	        BufferedWriter bw = new BufferedWriter(fw);
	        bw.write("@echo off");
	        bw.newLine();
	        bw.write("setlocal ENABLEDELAYEDEXPANSION");
	        bw.newLine();
	        bw.write("echo.");
	        bw.newLine();
	        bw.write("echo Fingerprint Server se está ejecutando en el puerto " +port);
	        bw.newLine();
	        bw.write(content);
	        bw.close();
	    } catch (Exception e) {
	        throw new Exception(e.getMessage());
	    }
	}
	
	private static String enableTask(boolean enabled) throws IOException {
		String task = "SchTasks /change /TN \""+nameTask+"\" /";
		task = enabled ? task + "ENABLE" : task + "DISABLE";
		return getOutPut(runTime(task));
	}
	
	private static String deleteTask() throws IOException {
		String task = "SchTasks /delete /TN \""+nameTask+"\" /F";
		return getOutPut(runTime(task));
	}
	
	private static void closeWindows() {
	 if (JOptionPane.showConfirmDialog(frmFingerprintServer, 
	            "¿Está seguro de cerrar el servidor?", "Cerrar", 
	            JOptionPane.YES_NO_OPTION,
	            JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION){
		 		runTime("taskkill /F /IM java.exe");
	            System.exit(0);
	        }
	}
}
